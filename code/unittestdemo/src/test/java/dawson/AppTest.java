package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    
    @Test
    public void shouldPrintX(){
        assertEquals("Testing method 'echo'", 5, App.echo(5));
    }

    @Test
    public void oneMore(){
        assertEquals("Testing method 'oneMore'", 6, App.oneMore(5));
    }
}
